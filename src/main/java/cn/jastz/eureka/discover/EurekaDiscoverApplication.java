package cn.jastz.eureka.discover;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EurekaDiscoverApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaDiscoverApplication.class, args);
	}
}
