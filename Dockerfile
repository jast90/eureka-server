FROM java:8-jre
MAINTAINER Zhiwen Zhang <zhangzhiwen91@gmail.com>

ADD ./target/eureka-discover-0.0.1-SNAPSHOT.jar /app/
CMD ["java", "-Xmx200m", "-jar", "/app/eureka-discover-0.0.1-SNAPSHOT.jar"]

EXPOSE 8761